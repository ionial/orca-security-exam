import sys
import json
import logging
from modules.stats_container import StatsContainer

logging.getLogger().setLevel(logging.DEBUG)

def test_stats_container():
    stats_container = StatsContainer()
    stats = stats_container.get_stats()
    assert stats.request_count == 0
    assert stats.average_request_time == 0

    stats_container.update(10, 1000)
    stats = stats_container.get_stats()
    assert stats.request_count == 10
    assert stats.average_request_time == 100

    stats_container.update(100, 1000)
    stats = stats_container.get_stats()
    assert stats.request_count == 110
    assert stats.average_request_time == float(2000)/float(110)

    stats_container.update(1, 1)
    stats = stats_container.get_stats()
    assert stats.request_count == 111
    assert stats.average_request_time == float(2001)/float(111)
