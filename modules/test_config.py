import sys
import json
import logging
import pytest
from modules import config

logging.getLogger().setLevel(logging.DEBUG)

def test_load_bad_config():
    conf = config.Config.from_json_str('''
            {vms":[{"vm_id":"vm-id","name":"vm-name","tags":["tag1","tag2"]}],
             "fw_rules":[{"fw_id":"fw-id","source_tag":"source-tag","dest_tag":"dest-tag"}]}
        ''')
    assert conf == None    
    conf = config.Config.from_json_str('''
            {
             "fw_rules":[{"fw_id":"fw-id","source_tag":"source-tag","dest_tag":"dest-tag"}]}
        ''')
    assert conf == None    

    conf = config.Config.from_json_str('''
            {"vms":[{"vm_id":"vm-id","name":"vm-name","tags":["tag1","tag2]}],
             "fw_rules":[{"fw_id":"fw-id","source_tag":"source-tag","dest_tag":"dest-tag"}]}
        ''')
    assert conf == None    

def test_vm():
    vm = config.VM.from_json(json.loads('{"vm_id":"vm-id","name":"vm-name","tags":["tag1","tag2"]}'))
    assert vm.vm_id == "vm-id"
    assert vm.name == "vm-name"
    assert len(vm.tags) == 2
    assert set(vm.tags) == set(["tag1","tag2"])

def test_fw_rule():
    fw_rule = config.FWRule.from_json(json.loads('{"fw_id":"fw-id","source_tag":"source-tag","dest_tag":"dest-tag"}'))
    assert fw_rule.fw_id == "fw-id"
    assert fw_rule.source_tag == "source-tag"
    assert fw_rule.dest_tag == "dest-tag"

def test_config_from_json_str():
    conf = config.Config.from_json_str('''
            {"vms":[{"vm_id":"vm-id","name":"vm-name","tags":["tag1","tag2"]}],
             "fw_rules":[{"fw_id":"fw-id","source_tag":"source-tag","dest_tag":"dest-tag"}]}
        ''')
    assert len(conf.vms) == 1
    assert len(conf.fw_rules) == 1
    assert "vm-id" in conf.vms and conf.vms["vm-id"].vm_id=="vm-id"
    assert "fw-id" in conf.fw_rules and conf.fw_rules["fw-id"].fw_id=="fw-id"

def test_config_from_json_file():
    conf = config.Config.from_json_file("modules/test_data/input-0.json")
    assert len(conf.vms) == 2
    assert len(conf.fw_rules) == 1
    assert "vm-c7bac01a07" in conf.vms
    vm = conf.vms["vm-c7bac01a07"]
    assert vm.vm_id=="vm-c7bac01a07"
    assert vm.name =="bastion"
    assert set(vm.tags) == set(["ssh", "dev"])
    assert "fw-82af742" in conf.fw_rules
    fw = conf.fw_rules["fw-82af742"]
    assert fw.fw_id=="fw-82af742"
    assert fw.source_tag =="ssh"
    assert fw.dest_tag == "dev"

def test_attack():
    logging.info("test_attack")
    conf = config.Config.from_json_file("modules/test_data/input-0.json")
    assert set(conf.naive_attack("vm-a211de")) == set(["vm-c7bac01a07"])
    assert set(conf.attack("vm-a211de")) == set(["vm-c7bac01a07"])
    assert set(conf.naive_attack("vm-c7bac01a07")) == set(["vm-c7bac01a07"])
    assert set(conf.attack("vm-c7bac01a07")) == set(["vm-c7bac01a07"])
    assert set(conf.naive_attack("non-existent-id")) == set([])
    assert set(conf.attack("non-existent-id")) == set([])

def test_naive_attack3():
    logging.info("test_attack")
    conf = config.Config.from_json_file("modules/test_data/input-3.json")
    assert set(conf.naive_attack("vm-864a94f")) == set([])
    assert set(conf.attack("vm-864a94f")) == set([])
    assert set(conf.naive_attack("vm-a3660c")) == set([])
    assert set(conf.attack("vm-a3660c")) == set([])
    assert set(conf.naive_attack("vm-0c1791")) == set([])
    assert set(conf.attack("vm-0c1791")) == set([])
    assert set(conf.naive_attack("vm-2987241")) == set([])
    assert set(conf.attack("vm-2987241")) == set([])
    assert set(conf.naive_attack("vm-ab51cba10")) == set(['vm-f00923', 'vm-d9e0825', 'vm-575c4a', 'vm-2987241', 'vm-9ea3998', 'vm-59574582', 'vm-a3660c', 'vm-5f3ad2b', 'vm-864a94f', 'vm-0c1791'])
    assert set(conf.attack("vm-ab51cba10")) == set(['vm-f00923', 'vm-d9e0825', 'vm-575c4a', 'vm-2987241', 'vm-9ea3998', 'vm-59574582', 'vm-a3660c', 'vm-5f3ad2b', 'vm-864a94f', 'vm-0c1791'])

def test_union_list_of_sets():
    assert config.union_list_of_sets([]) == set([])
    assert config.union_list_of_sets([[],[]]) == set([])
    assert config.union_list_of_sets([set([]),set([])]) == set([])

    assert config.union_list_of_sets([[1],[1]]) == set([1])
    assert config.union_list_of_sets([set([1]),set([1])]) == set([1])

    assert config.union_list_of_sets([[1],[2]]) == set([1,2])
    assert config.union_list_of_sets([set([1]),set([2])]) == set([1,2])

    assert config.union_list_of_sets([[1,2],[2]]) == set([1,2])
    assert config.union_list_of_sets([set([1,2]),set([2])]) == set([1,2])

    assert config.union_list_of_sets([[1],[2,1]]) == set([1,2])
    assert config.union_list_of_sets([set([1]),set([2,1])]) == set([1,2])
