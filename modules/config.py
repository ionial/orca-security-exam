import json
import logging
from functools import reduce
from modules.utilities import log_exception

def union_list_of_sets(sets):
    ''' combines list of sets into a single set '''
    return reduce((lambda x,y: set(x).union(set(y))),sets,set([]))

class VM:
    def __init__(self, vm_id, name, tags):
        self.vm_id = vm_id
        self.name = name
        self.tags = tags

    @classmethod
    def from_json(clazz, data):
        return clazz(**data)

class FWRule(object):
    def __init__(self, fw_id, source_tag, dest_tag):
        self.fw_id = fw_id
        self.source_tag = source_tag
        self.dest_tag = dest_tag

    @classmethod
    def from_json(clazz, data):
        return clazz(**data)
    
class Config(object):
    def __init__(self, vms, fw_rules):
        self.vms = vms
        self.fw_rules = fw_rules
        # for faster access -- initially we can use dict, if dict becomes too big
        # we can start to shard it
        self.tags_to_vms = {}
        self.dest_to_source = {}

    @classmethod
    def _from_json(clazz, data):
        try:
            vms = {x.vm_id:x for x in list(map(VM.from_json, data["vms"]))}
            fw_rules = {x.fw_id:x for x in list(map(FWRule.from_json, data["fw_rules"]))}
            config =  clazz(vms, fw_rules)
            config._post_process_config()
            return config
        except BaseException as ex:
            log_exception(ex)
            return None

    @classmethod
    def from_json_str(clazz, json_str):
        try:
            parsed_json = json.loads(json_str)
            return Config._from_json(parsed_json)
        except BaseException as ex:
            log_exception(ex)
            return None

    @classmethod
    def from_json_file(clazz, json_file):
        try:
            logging.info("Loading from file '{}'".format(json_file))
            parsed_json = json.load(open(json_file))
            return Config._from_json(parsed_json)
        except BaseException as ex:
            if isinstance(ex, Exception):
                logging.error("[{}] Exception: {}".format(_ex_frame(), ex))
            else:
                logging.error("[{}] {}".format(_ex_frame(), ex))
            return None

    def _map_tags_to_vms(self):
        """ Generates reverse mapping of tags to vm-ids"""
        tags_to_vms = {}
        for vm in self.vms.values():
            for tag in vm.tags:
                if tag in tags_to_vms:
                    tags_to_vms[tag] = set(tags_to_vms[tag]).union(set([vm.vm_id]))
                else:
                    tags_to_vms[tag] = set([vm.vm_id])
        return tags_to_vms

    def _map_dest_to_source(self):
        """ Generates a mapping of sources that can reach destination"""
        dest_to_source = {}
        for fw in self.fw_rules.values():
            if fw.dest_tag in dest_to_source:
                dest_to_source[fw.dest_tag] = dest_to_source[fw.dest_tag].union(set([fw.source_tag]))
            else:
                dest_to_source[fw.dest_tag] = set([fw.source_tag])
        return dest_to_source

    def _post_process_config(self):
        """ Generates reverse mapping for faster attacker searches"""
        self.tags_to_vms = self._map_tags_to_vms()
        self.dest_to_source = self._map_dest_to_source()

    def naive_attack(self, vm_id):
        """ For vm_id returns list of vm_ids who can be sources to reach the designated vm based
            on source_tag--dest_tag firewall rules 
            vm_id : vm id
            Returns: list of vm_ids. if vm_id is unknown returns an empty list
        """
        # naive implementation. in case we have many vms and many fw rules the loops may become long
        if vm_id not in self.vms:
            logging.warning("vm_id={} is not in configuraion".format(vm_id))
            return []

        attacking_tags = set([fw.source_tag for fw in self.fw_rules.values() if fw.dest_tag in self.vms[vm_id].tags])
        logging.debug("vm_id={} attacking_tags = {}".format(vm_id, attacking_tags))
        attackers =  [ vm.vm_id for vm in self.vms.values() if not set(vm.tags).isdisjoint(attacking_tags) ]
        logging.debug("vm_id={} attackers = {}".format(vm_id, attackers))
        return attackers

    def attack(self, vm_id):
        """ For vm_id returns list of vm_ids who can be sources to reach the designated vm based
            on source_tag--dest_tag firewall rules 
            vm_id : vm id
            Returns: list of vm_ids. if vm_id is unknown returns an empty list
        """
        # naive implementation. in case we have many vms and many fw rules the loops may become long
        if vm_id not in self.vms:
            logging.warning("vm_id={} is not in configuraion".format(vm_id))
            return []

        attacking_tags = union_list_of_sets([self.dest_to_source[tag] for tag in self.vms[vm_id].tags if tag in self.dest_to_source])
        logging.debug("vm_id={} attacking_tags = {}".format(vm_id, attacking_tags))
        attackers =  [ vm.vm_id for vm in self.vms.values() if not set(vm.tags).isdisjoint(attacking_tags) ]
        logging.debug("vm_id={} attackers = {}".format(vm_id, attackers))
        return attackers
