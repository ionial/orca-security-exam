import logging
import threading

class StatsContainer:
    def __init__(self):
        self.request_count = 0
        self.total_time = 0
        self.update_lock = threading.Lock()
        self.read_only_copy = self.copy_for_read()

    def copy_for_read(self):
        class NS:
            pass
        ns = NS()
        ns.request_count = self.request_count
        ns.average_request_time = float(self.total_time) / float(self.request_count) if float(self.request_count !=0) else 0
        return ns

    def update(self, request_count, elapsed_time):
        self.update_lock.acquire()
        self.request_count += request_count
        self.total_time += elapsed_time
        logging.debug("elapsed_time={} , request_count={}, total_time={}".format(elapsed_time, self.request_count, self.total_time))
        self.read_only_copy = self.copy_for_read()
        self.update_lock.release()

    def get_stats(self):
        return self.read_only_copy
