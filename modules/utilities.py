import logging
import traceback

def log_exception(ex):
    logging.error(traceback.format_exc())
