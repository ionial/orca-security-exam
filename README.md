Orca-Securitty test solution

Files:
setup.sh -- should be run as 'source ./setup.sh'. It configures the entire environment with all expected packages
pytest.sh -- runs py.test for the written code 
service.py -- the webservice.
test_service.py -- test tool that accesses the webservice from multiple processes and then validates statistics
                    to be correct

modules/config.py -- packages that peforms the work on top of the configuration -- loading and calculation
modules/stats_container.py -- thread-safe statistics collector for requests and their times
modules/test_*.py -- unittests