#!/usr/bin/env python

import sys
import argparse
import json
import logging
import time
import requests
import multiprocessing
import subprocess
import shlex

def run_service():
    p = subprocess.Popen(shlex.split("python ./service.py ./modules/test_data/input-3.json -vv -l test_service_service.log"))
    time.sleep(5)

def stop_service(service_handle):
    service_handle.kill()

def run_single_process(request_count):
    for _ in range(0, request_count):
        requests.get("http://localhost:8000/api/v1/attack?vm_id=vm-ab51cba10")

def run_multi_processes(process_num, request_count):
    procs = []
    for _ in range(0, process_num):
        procs.append(multiprocessing.Process(target=run_single_process, args=(request_count,)))
    
    for p in procs:
        p.start()
    for p in procs:
        p.join()

def get_stats():
    try:
        response = requests.get("http://localhost:8000/api/v1/stats")

        logging.debug("HTTP Response: {} {}".format(response.status_code, response.text))

        if response.status_code != requests.codes.ok:
            raise BaseException("Bad response code: {}".format(response.status_code))
        result = json.loads(response.text)
        logging.debug("Got:{}".format(result))
        return result['request_count']
    except Exception as ex:
        logging.error("failed to get statistics {}", ex)
        raise BaseException(ex)

def validate_stats(expected_requests):
    try:
        request_count = get_stats()
        if request_count != expected_requests:
            return (False, "Expected {} requests, got {}".format(expected_requests, request_count))
        return (True,"")
    except Exception as ex:
        logging.error("failed to get statistics {}", ex)
        return (False, "general error")

def main():
    parser = argparse.ArgumentParser(description="Service Test Parallel Runner")
    parser.add_argument("-p", "--processes", dest="process_num", default=1, type=int)
    parser.add_argument("-n", "--requests", dest="request_count", default=1, type=int)
    parser.add_argument("-r", "--run_service", dest="run_service", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", help="Run with debug logging", dest="verbose", action="count", default=0)

    args = parser.parse_args()
    if args.verbose > 1:
        logging.getLogger().setLevel(logging.DEBUG)
    elif args.verbose > 0:
        logging.getLogger().setLevel(logging.INFO)
    else:
        logging.getLogger().setLevel(logging.WARNING)

    local_service = None
    if args.run_service:
        local_service = run_service()

    base_stats_count = get_stats()

    if args.process_num != 1:
        run_multi_processes(args.process_num, args.request_count)
    else:
        run_single_process(args.request_count)

    # expected requests 
    expected_requests = args.process_num * args.request_count + base_stats_count + 1
    status, reason = validate_stats(expected_requests)
    if local_service:
        stop_service(local_service)

    if not status:
        logging.error("Validation failed:{}".format(reason))
        sys.exit(1)
    return 0

if __name__ == "__main__":
    sys.exit(main())
