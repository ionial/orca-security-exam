#!/usr/bin/env python
import sys
import logging
import time
import json
import argparse
import flask
from flask import Flask
from flask import request
from flask import request_started
from flask import request_finished
from modules import config
from modules.stats_container import StatsContainer

app = Flask(__name__)

stats_container = StatsContainer()
global vm_conf

def on_request_start(sender, **extra):
    flask.g.start_perf=time.perf_counter()

def on_request_finish(sender, response, **extra):
    elapsed = time.perf_counter() - flask.g.start_perf
    stats_container.update(1, elapsed)

request_started.connect(on_request_start, app)
request_finished.connect(on_request_finish, app)

def json_response(object, return_code=200):
    return flask.Response(json.dumps(object), status=return_code, mimetype='application/json')

@app.route('/api/v1/attack')
def attack():
    # /api/v1/attack?vm_id=<id>
    vm_id = request.args.get('vm_id')
    if vm_id is None:
        logging.error("missing vm_id")
        return flask.abort(400)
    attacking_vms = vm_conf.attack(vm_id)
    return json_response(attacking_vms)

@app.route('/api/v1/stats')
def stats():
    stat = {}
    requests_stats = stats_container.get_stats()
    stat['vm_count'] = len(vm_conf.vms)
    stat['request_count'] = requests_stats.request_count
    stat['average_request_time'] = requests_stats.average_request_time
    return json_response(stat)

def run_app():
    parser = argparse.ArgumentParser(description="WebService")
    parser.add_argument("-c", "--config", help="configuration file", required=True, dest="config_file")
    parser.add_argument("-v", "--verbose", help="Run with debug logging", dest="verbose", action="count", default=0)
    parser.add_argument("-l", "--log-file", help="Save log to file", dest="log_file", default=None)
    args = parser.parse_args()
    debug = args.verbose > 1
    if args.verbose > 1:
        log_level = logging.DEBUG
    elif args.verbose > 0:
        log_level = logging.INFO
    else:
        log_level = logging.WARNING
    if args.log_file:
        logging.basicConfig(filename=args.log_file,level=log_level)
    else:
        logging.basicConfig(level=log_level)
    
    global vm_conf    
    vm_conf = config.Config.from_json_file(args.config_file)
    if vm_conf == None:
        logging.error("Failed to load configuration from file {}".format(args.config_file))
        sys.exit(1)
    app.run(debug=debug, port=8000)

if __name__ == '__main__':
    run_app()