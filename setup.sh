python3 -m venv env
. ./env/bin/activate
pip install --upgrade pip --isolated
pip install flask --isolated
pip install ipython --isolated
pip install pytest --isolated
pip install blinker --isolated
pip install requests --isolated
